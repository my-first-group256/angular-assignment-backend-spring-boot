package com.restaurant.assignment.controller;

import com.restaurant.assignment.entity.Dish;
import com.restaurant.assignment.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/dish/")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping("/")
    public ResponseEntity<Dish> addDish(@RequestBody Dish dish) {
        try {
            Dish savedDish = dishService.addDish(dish);
            return ResponseEntity.ok(dish);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<Dish>> addDish() {
        try {
            List<Dish> allDishes = dishService.getAllDishes();
            return ResponseEntity.ok(allDishes);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/{city}")
    public ResponseEntity<List<Dish>> addDish(@PathVariable("city") String city) {
        try {
            List<Dish> dataByCity = dishService.getDataByCity(city);
            return ResponseEntity.ok(dataByCity);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
