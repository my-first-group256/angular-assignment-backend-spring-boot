package com.restaurant.assignment.service;

import com.restaurant.assignment.entity.Dish;
import com.restaurant.assignment.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishService {

    @Autowired
    private DishRepository dishRepository;

    public List<Dish> getAllDishes() {
       return dishRepository.findAll();
    }

    public Dish addDish(Dish dish) {
        return dishRepository.save(dish);
    }

    public List<Dish> getDataByCity(String city) {
        return dishRepository.getByCity(city);
    }
}
